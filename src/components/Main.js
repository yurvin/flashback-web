import React, { Component } from 'react';
import Header from './header/Header';
import CardsMenu from './cards/CardsMenu/CardsMenu';
import Table from './table/Table';
import '../App.css';

class Main extends Component {

  constructor(props){
    super(props);

    let words = [];
    this.state = { isCard: false };

    this.createRow = this.createRow.bind(this);
    this.updateRow = this.updateRow.bind(this);
    this.deleteRow = this.deleteRow.bind(this);

    words.forEach((word) => {
      //ToDo: needs to move a creation id and empty field "transcription" to server
      word.id = Math.random();
      word.selected = false;
      word.isEditText = false;
      if (!word.transcription) word.transcription = '';
    });

    this.state.words = words;

    this.scrollToBottom = false;
    if(words.length>0) this.lastRowId = words[words.length-1].id;
  }

  changeView = () => {
    this.setState({
      isCard: !this.state.isCard
    })
  };

  componentDidMount() {
    this.props.serverApi.CRUD.read(this.props.user).then(words => {
      this.setState({ words: words });
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.scrollToBottom){
      let element = document.getElementById(this.lastRowId);
      if(element){
        element.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
      }
      this.scrollToBottom = false;
    }
  }

  createRow = () => {
    let { words } = this.state;

    words.forEach(word => word.selected = false);

    //ToDo: needs to move a creation id and empty field "transcription" to server
    words.push({
      eng: '',
      transcription: '',
      rus: '',
      id: Math.random(),
      selected: true,
      isEditText: true
    });

    this.scrollToBottom = true;
    this.lastRowId = words[words.length-1].id;

    this.setState({ words: words });
  };

  updateRow = () => {
    this.props.serverApi.CRUD.update(this.props.user, this.state.words);
  };

  deleteRow = (id) => {
    let { words } = this.state;
    let index = words.findIndex(word => word.id === id);

    if(index >= 0){
      words.splice(index, 1);
      this.setState({ words: words });
    }

    this.props.serverApi.CRUD.update(this.props.user, words);
  };

  render() {
    return (
      <div className="container">
        <Header
          isCard={this.state.isCard}
          changeView={this.changeView}
          exitSession={this.props.exitSession}
        />
        { this.state.isCard ?
          <CardsMenu words={this.state.words}/> :
          <Table
            createRow={this.createRow}
            updateRow={this.updateRow}
            deleteRow={this.deleteRow}
            words={this.state.words}
          /> }
      </div>
    );
  }
}

export default Main;
