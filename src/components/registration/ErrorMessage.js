import React from 'react';

export default (props) => {
  if(props.isError){
    return  (
      <div className="alert alert-danger" role="alert">
        {props.errorMessage}
      </div>
    )
  }

  return null;
}