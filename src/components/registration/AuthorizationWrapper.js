import React, { Component } from 'react';
import Authorization from './Authorization';
import Registration from './Registration';
import PropTypes from 'prop-types';
import './Auth.css';

export default class AuthorizationWrapper extends Component {
  state = {
    view: 'authorization',
    error: this.props.isError
  };

  changeView = (view, options) => {
    this.setState({
      view: view,
      error: options && options.error
    });
  };

  gotoRegistration = () => {
    this.changeView('registration');
  };

  gotoAuthorization = () => {
    this.changeView('authorization');
  };

  static getDerivedStateFromProps(nextProps, prevState){
    prevState.error = nextProps.isError;
    return { ...prevState };
  }

  render() {
    return this.state.view.toLowerCase() === 'authorization' ?
      <Authorization
        isError={ this.state.error }
        authorizationApi={ this.props.serverApi.authorization }
        updateUser={ this.props.updateUser }
        changeView={ this.changeView }
      /> :
      <Registration
        isErrLogin={ this.state.error }
        registrationApi={ this.props.serverApi.registration }
        updateUser={ this.props.updateUser }
        changeView={ this.changeView }
      />
  }
}

// AuthorizationWrapper.propTypes = {
//   serverApi: {
//     authorization: PropTypes.func.isRequired,
//     registration: PropTypes.func.isRequired
//   }
// };

Authorization.propTypes = {
  authorizationApi: PropTypes.func.isRequired
};

Registration.propTypes = {
  registrationApi: PropTypes.func.isRequired
};