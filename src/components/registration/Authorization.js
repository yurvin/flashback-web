import React, { Component } from 'react';
import ErrorMessage from './ErrorMessage';
import constants from '../../utils/constants';

export default class Authorization extends Component{
  state = {
    username: '',
    password: '',
    isError: this.props.isError,
    errorMessage: constants.authorization.errMsg.ru,
    phLogin: constants.authorization.phLogin.ru,
    phPass: constants.authorization.phPass.ru,
    name: constants.authorization.name
  };

  handleLogin = (e) => {
    this.setState({ username: e.target.value });
  };

  handlePassword = (e) => {
    this.setState({ password: e.target.value });
  };

  sendData = () => {
    this.props.authorizationApi(this.state.username, this.state.password, this.props.updateUser);
  };

  gotoRegistration = () => {
    this.props.changeView(constants.registration.name);
  };

  static getDerivedStateFromProps(nextProps, prevState){
    prevState.isError = nextProps.isError;
    return { ...prevState };
  }

  render() {
    return (
      <div className={"container-fluid h-100 auth-background pos-fixed text-dark m-30"}>
        <form className={"col-lg-4 col-sm-6 col-xs-12 mt-30 margin-auto"} onSubmit={(e)=>{e.preventDefault(); return false;}}>
          <ErrorMessage isError={this.state.isError} errorMessage={this.state.errorMessage}/>
          <div className={"form-group"}>
            <input
              id={"inputLogin"}
              className={"form-control"}
              type="text"
              placeholder={this.state.phLogin}
              onChange={ this.handleLogin }
            />
          </div>
          <div id={"form-group"}>
            <input
              id={"inputPassword"}
              className={"form-control"}
              type="password"
              placeholder={this.state.phPass}
              onChange={ this.handlePassword }
            />
          </div>
          <div className={"form-group pt-2"}>
            <a href="#" className="badge badge-light" onClick={this.gotoRegistration}>Registration</a>
            <button type="submit" className="float-right btn btn-light" onClick={ this.sendData.bind(this) }>Sign in</button>
          </div>
        </form>
      </div>
    );
  }
};