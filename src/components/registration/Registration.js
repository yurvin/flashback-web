import React, { Component } from 'react';
import ErrorMessage from './ErrorMessage';
import constants from '../../utils/constants';

export default class Registration extends Component{
  state = {
    username: '',
    password: '',
    passwordAgain: '',
    isErrLogin: !!this.props.isErrLogin,
    isErrPass: false,
    errMsgLoginServer: constants.registration.errMsgLoginServer.ru,
    errMsgLoginClient: constants.registration.errMsgLoginClient.ru,
    errMsgPass: constants.registration.errMsgPass.ru,
    phLogin: constants.registration.phLogin.ru,
    phPass: constants.registration.phPass.ru,
    phPassAgain: constants.registration.phPassAgain.ru,
    name: constants.authorization.name
  };

  handleLogin = (e) => {
    this.setState({ username: e.target.value });
  };

  handlePass = (e) => {
    this.setState({ password: e.target.value });
  };

  handlePassAgain = (e) => {
    this.setState({ passwordAgain: e.target.value });
  };

  checkLogin = () => {
    let check = this.state.username.length > 0;
    this.setState({ isErrLogin: !check });
    return check;
  };

  checkPassword = () => {
    let match = (this.state.password === this.state.passwordAgain) &&
                this.state.password.length>0 &&
                this.state.passwordAgain.length>0;

    this.setState({ isErrPass: !match });
    return match;
  };

  sendData = () => {
    if(!this.checkLogin() || !this.checkPassword()) return;

    this.props.registrationApi(this.state.username, this.state.password, (response)=>{
      if(response.status===409){
        this.props.changeView(constants.registration.name, {error: 409});
      } else {
        this.props.updateUser(response);
      }
    });
  };

  gotoAuthorization = () => {
    this.props.changeView(constants.authorization.name);
  };

  static getDerivedStateFromProps(nextProps, prevState){
    prevState.isErrLogin = nextProps.isErrLogin;
    return { ...prevState };
  }

  render() {
    return (
      <div className={"container-fluid h-100 auth-background pos-fixed text-dark m-30"}>
        <form className={"col-lg-4 col-sm-6 col-xs-12 mt-30 margin-auto"} onSubmit={(e)=>{e.preventDefault(); return false;}}>
          <ErrorMessage
            isError={this.state.isErrLogin}
            errorMessage={this.props.isErrLogin ? this.state.errMsgLoginServer : this.state.errMsgLoginClient}
          />
          <div className={"form-group"}>
            <input
              id={"inputLogin"}
              className={"form-control"}
              type="text"
              placeholder={this.state.phLogin}
              onChange={ this.handleLogin }
            />
          </div>
          <ErrorMessage isError={this.state.isErrPass} errorMessage={this.state.errMsgPass}/>
          <div id={"form-group"}>
            <input
              id={"inputPassword"}
              className={"form-control"}
              type="password"
              placeholder={this.state.phPass}
              onChange={ this.handlePass }
            />
          </div>
          <div id={"form-group mt-2"}>
            <input
              id={"inputPassword"}
              className={"form-control"}
              type="password"
              placeholder={this.state.phPassAgain}
              onChange={ this.handlePassAgain }
              onBlur={ this.checkPassword }
            />
          </div>
          <div className={"form-group pt-2"}>
            <a href="#" className="badge badge-light" onClick={this.gotoAuthorization}>Authorization</a>
            <button type="submit" className="float-right btn btn-light" onClick={ this.sendData.bind(this) }>Check in</button>
          </div>
        </form>
      </div>
    );
  }
}