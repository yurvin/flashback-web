import React, { Component } from 'react';
import Thead from './Thead';
import Tr from './Tr';
import Octicon, {Check, DiffAdded} from '@githubprimer/octicons-react';

export default class Table extends Component {

  constructor(props){
    super(props);
    this.state = {...props};
  }

  static getDerivedStateFromProps(nextProps, prevState){
    return {
      words: nextProps.words
    }
  }

  setChoose = (id) => {
    let { words } = this.state;

    words.map((word) => {
      word.selected = false;
      word.isEditText = false;

      if(word.id === id){
        word.selected = true;
      }
    });

    this.setState({ words: words });
  };

  resetChoose = () => {
    let { words } = this.state;

    words.map((word) => {
      word.selected = false;
      words.isEditText = false;
    });
    this.setState({ words: words });
  };

  render() {
    return (
      <div className={"table-responsive overflow-x-hidden overflow-y-hidden"}>
        <table className={"table table-striped"}>
          <Thead/>
          <tbody className={"tbody"}>
          {this.state.words.map((word, index) => {
            return <Tr
                      key={word.id}
                      word={word}
                      index={index+1}
                      isHeader={false}
                      setChoose={this.setChoose}
                      resetChoose={this.resetChoose}
                      updateRow={this.state.updateRow}
                      deleteRow={this.state.deleteRow}
                    />
            })
          }
          </tbody>
        </table>
        <button type={"button"} className={"w-100 btn btn-success"} onClick={this.state.createRow}>
          <Octicon className={"white"} icon={DiffAdded} mega/>
        </button>
      </div>
    );
  }
}