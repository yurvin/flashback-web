import React, {Component} from 'react';
import { Swipeable } from 'react-touch';
import Octicon, { Check, Pencil, Trashcan } from '@githubprimer/octicons-react'; //https://insin.github.io/react-octicon/
import './table.css';

const Header = (props) => {
  return (
    <tr>
      <th className={"w-25px"} scope={"scope"}> № </th>
      <th scope={"scope"}> word </th>
      <th className={"d-none d-sm-table-cell"} scope={"scope"}> transcription </th>
      <th scope={"scope"}> translation</th>
    </tr>
  )
};

/**
 * like CRUD without "READ"
 * wrapper for components <BtnsEditDelete/> and <BtnUpdate/>
 * @param props
 * @constructor
 */
const BtnsRow = (props) => {
  if(props.isSelected && props.isEditText){
    return <td className={"p-0 h-100px"}><BtnApprove selectItem={props.selectItem}/></td>
  } else if (props.isSelected && !props.isEditText) {
    return <td className={"p-0 h-100px"}><BtnsEditDelete selectItem={props.selectItem}/></td>
  } else {
    return null;
  }
};

const BtnApprove = (props) => {
  let approve = () => props.selectItem('update');

  return (
    <div onClick={approve} className={"bg-success float-right h-100 w-100"}>
      <Octicon className={"svg-center white"} icon={Check} mega/>
    </div>
  )
};

const BtnsEditDelete = (props) => {
  let edit = () => props.selectItem('edit');
  let remove = () => props.selectItem('delete');

  return (
      <div className={"h-100"}>
        <div onClick={edit} className={"d-inline-block bg-success h-100 w-50"}>
          <Octicon className={"svg-center white"} icon={Pencil} mega/>
        </div>
        <div onClick={remove} className={"d-inline-block bg-danger h-100 w-50"}>
          <Octicon className={"svg-center white"} icon={Trashcan} mega/>
        </div>
      </div>
  );
};

class TdInput extends Component {
  handler = ({ target: { value } }) => {
    this.props.handler(value);
  };

  render() {
    if(this.props.isEditText){
      return <input type="text" value={ this.props.value } onChange={this.handler.bind(this)} />
    } else {
      return <span>{ this.props.value }</span>
    }
  }
}

class TdArea extends Component {
  handler = ({ target: { value } }) => {
    this.props.handler(value);
  };

  render() {
    if(this.props.isEditText){
      return <textarea
                name=""
                id=""
                rows="3"
                onChange={this.handler.bind(this)}
                value={ this.props.value }
              />
    } else {
      return <span>{ this.props.value }</span>
    }
  }
}


export default class Tr extends Component {

  constructor(props){
    super(props);
    this.state = {...props};

    this.swipeLeft = this.swipeLeft.bind(this);
    this.swipeRight = this.swipeRight.bind(this);

    this.chooseAction = this.chooseAction.bind(this);
    this.handlerEng = this.handlerEng.bind(this);
    this.handlerTranscription = this.handlerTranscription.bind(this);
    this.handleRus = this.handleRus.bind(this);
  }

  swipeLeft = () => {
    this.props.setChoose(this.props.word.id);
  };

  swipeRight = () => {
    this.props.resetChoose();
  };

  chooseAction = (item) => {
    let { word } = this.state;

    switch (item) {
      case 'edit':
        word.isEditText = true;
        this.setState({ word: word });
        break;

      case 'delete':
        this.props.deleteRow(word.id);
        this.props.resetChoose();
        break;

      case 'update':
        word.isEditText = false;
        this.setState({ word: word });
        this.props.updateRow(this.state.word.id);
        this.props.resetChoose();
        break;

      default:
        break;
    }
  };

  handler = (field, value) => {
    let { word } = this.state;
    word[field] = value;

    this.setState({ word: word })
  };

  handlerEng = (value) => {
    this.handler('eng', value);
  };

  handlerTranscription = (value) => {
    this.handler('transcription', value);
  };

  handleRus = (value) => {
    this.handler('rus', value);
  };

  static getDerivedStateFromProps(nextProps, prevState){
    return { ...nextProps };
  }

  render() {
    let {word, index} = this.state;

    if(this.state.isHeader){
      return <Header/>
    }

    return (
      <tr id={this.state.word.id}>
        <th className={"w-25px"} scope={"row"}>{index}</th>
        <td>
          <TdInput isEditText={word.isEditText} value={word.eng} handler={this.handlerEng}/>
        </td>
        <td className={"d-none d-sm-table-cell"}>
          <TdInput isEditText={word.isEditText} value={word.transcription} handler={this.handlerTranscription}/>
        </td>
        <Swipeable onSwipeLeft={this.swipeLeft} onSwipeRight={this.swipeRight}>
          <td className={"p-0"}>
            <div className={"min-height-60"}>
              <TdArea isEditText={word.isEditText} value={word.rus} handler={this.handleRus}/>
            </div>
          </td>
          </Swipeable>
        <BtnsRow
          selectItem={this.chooseAction}
          isEditText={word.isEditText}
          isSelected={word.selected}
        />
      </tr>
    );
  }
}