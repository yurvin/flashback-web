import React from 'react';
import Tr from './Tr';

const Thead = () => {
  return (
    <thead>
      <Tr isHeader={true}/>
    </thead>
  );
};

export default Thead;