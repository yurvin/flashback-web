import React from 'react';
import ToggleBtn from '../toggleButton/ToggleBtn';
import ExitBtn from '../exitButton/ExitBtn';
import CONSTANTS from '../../utils/constants.json'

const Header = (props) => {
  const title = CONSTANTS.header.title.ru;
  return (
    <div className="row align-content-end alert alert-primary">
      <div className="col-8 my-auto">
        <h1>{title}</h1>
      </div>
      <div className="col-3">
        <ToggleBtn {...props}/>
      </div>
      <div className={"col-1"}>
        <ExitBtn {...props}/>
      </div>
    </div>
  );
};

export default Header;