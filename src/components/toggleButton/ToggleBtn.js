import React from 'react';
import Checkbox from './Checkbox';
import CONSTANTS from '../../utils/constants.json';

const ToggleBtn = (props) => {

  return (
    <span>
      <Checkbox
        visible={true}
        onChange={props.changeView}
        text={CONSTANTS.header.cardsCb.ru}
      />
    </span>
  );
};

export default ToggleBtn;