import React from 'react';
import './checkbox.css';

const Checkbox = (props) => {
  return (
    <div className={props.visible ? "d-block" : "d-none"}>
      <label className="form-switch">
        <input onChange={props.onChange} type="checkbox"></input>
        <i></i>
        {props.text}
      </label>
    </div>
  )
};

export default Checkbox;