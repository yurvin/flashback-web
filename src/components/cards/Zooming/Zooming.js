import React from 'react';
import './zooming.css';

const   Zooming = (props) => {
  return (
    <div className={props.isZoom ? "zooming zoom" : ""}>
     { props.children }
    </div>
  );
};

export default Zooming;