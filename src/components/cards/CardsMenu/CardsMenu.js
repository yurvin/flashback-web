import React, {Component} from 'react';
import Training from '../Training/Training';
import Test from '../Test/Test';
import './cardsMenu.css';

export default class CardsMenu extends Component {
  state = {
    Training: true,
    Test: true,
    mode: null
  };

  setChoose = (component) => {
    let state = {...this.state};

    for(let key in state){
      state[key] = key === component;
      state.mode = component;
    }

    this.setState({...state});
  };

  render() {
    return (
      <div className={"row"}>
        <div className={"col-md-6 col-xs-12"}>
          <Training
            name={"Training"}
            setChoose={this.setChoose}
            visible={this.state.Training}
            position={"left"}
            mode={this.state.mode}
            {...this.props}
          />
        </div>

        <div className={"col-md-6 col-xs-12"}>
          <Test
            name={"Test"}
            setChoose={this.setChoose.bind(this)}
            visible={this.state.Test}
            position={"right"}
            mode={this.state.mode}
            {...this.props}
          />
        </div>
      </div>
    );
  }
}