import React from 'react';
import './cardWord.css';

const CardWord = (props) => {
  return (
    <div className={"card-body text-center card-face " + props.side}>
      <h1 className="card-text cardWord align-content-center">
        {props.word[props.lang]}
      </h1>
    </div>
  );
};

export default CardWord;