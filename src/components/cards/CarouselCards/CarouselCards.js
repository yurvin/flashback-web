import React, {Component} from 'react';
import MainCard from '../MainCard/MainCard';
import Slider from "react-slick"; //https://github.com/akiran/react-slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default class CarouselCards extends Component {

  constructor(props){
    super(props);

    this.settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: (ix) =>{
        //rotate all card to front side
        this.props.setChoose("front");
      }
    }

     // {this.state.isFlipped, this.state.header, this.state.position, this.state.selected} = ...props;
    this.state = {...props};
  }

  UNSAFE_componentWillUpdate(nextProps, nextState){
    this.state = {...nextProps};
  }

  render() {
    return (
      <Slider {...this.settings}>
        {
          this.props.words.map((word, ix) => {
            return (
              <div key={"word_"+ix}>
                <MainCard
                  word={word}
                  isFlipped={this.state.isFlipped}
                  header={this.state.header}
                  setChoose={this.props.setChoose.bind(this)}
                  position={this.state.position}
                  selected={this.state.selected}
                  fullScreen={true}
                />
                <img src="#" alt=""/>
              </div>
            )
          })
        }
      </Slider>
    );
  }
}