import React, { Component } from 'react';
import MainCard from '../MainCard/MainCard';
import CarouselCards from '../CarouselCards/CarouselCards';
import CONSTANTS from '../../../utils/constants.json';
import imgBackground from './training.png';
import Zooming from '../Zooming/Zooming';
import './training.css';

export default class Training extends Component {

  state = {
    header: CONSTANTS.cards.training.header.ru,
    words: this.props.words,
    zoom: false,
    isFlipped: false,
    chooseEvent: null,
    position: this.props.position,
    selected: !!this.props.mode
  };

  /**
   * @param sideForAll {String} "front" or "back"
   */
  setChoose = (sideForAll) => {
    let isFlipped = null;
    sideForAll = typeof sideForAll === "string" ? sideForAll : '';

    switch (sideForAll.toLowerCase()) {
      case "front":
        isFlipped = true;
        break;
      case "back":
        isFlipped = false;
        break;
      default:
        isFlipped = !this.state.isFlipped
    }

    this.setState({
      zoom: true,
      isFlipped: isFlipped,
      selected: true
    });

    this.props.setChoose(this.props.name);
  };

  render() {
    if(!this.props.visible) {
      return null;
    }

    if(this.state.selected){
      return (
        <Zooming isZoom={this.state.zoom}>
          <CarouselCards
            words={this.state.words}
            isFlipped={this.state.isFlipped}
            header={this.state.header}
            setChoose={this.setChoose.bind(this)}
            position={this.state.position}
            selected={this.state.selected}
          />
        </Zooming>
      );
    }

    return (
      <Zooming isZoom={this.state.zoom}>
        <MainCard
          isFlipped={this.state.isFlipped}
          word={this.state.words[0]}
          header={this.state.header}
          imgBackground={imgBackground}
          setChoose={this.setChoose.bind(this)}
          position={this.state.position}
          selected={this.state.selected}
        />
      </Zooming>
    );
  }
}