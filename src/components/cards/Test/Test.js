import React, { Component } from 'react';
import MainCard from '../MainCard/MainCard';
import CONSTANTS from '../../../utils/constants.json';
import Zooming from '../Zooming/Zooming';
import './test.css';
import imgBackground from './test.png';

export default class Test extends Component {

  state = {
    header: CONSTANTS.cards.test.header.ru,
    zoom: false,
    isFlipped: false,
    chooseEvent: null,
    position: this.props.position
  };

  setChoose = () => {
    console.log('choose test');
    this.setState({
      zoom: !this.state.zoom,
      isFlipped: !this.state.isFlipped
    });

    this.props.setChoose(this.props.name);
  };

  render() {
    if(!this.props.visible) {
      return null;
    }

    return (
      <Zooming isZoom={this.state.zoom}>
        <MainCard
          isFlipped={this.state.isFlipped}
          word={this.state.word}
          header={this.state.header}
          imgBackground={imgBackground}
          setChoose={this.setChoose.bind(this)}
          position={this.state.position}
        />
      </Zooming>
    );
  }
}