import React from 'react';

const CardImage = (props) => {
  return (
    <div className="card-body text-center card-face front">
      <img className={"rounded image-background"} src={ props.src } alt=""/>
    </div>
  );
};

export default CardImage;