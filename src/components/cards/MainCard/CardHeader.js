import React from 'react';
import Octicon, {ChevronRight} from '@githubprimer/octicons-react'; //https://insin.github.io/react-octicon/

const CardHeader = (props) => {
  if (props.arrow){
    return <div className="card-header">
      <Octicon className={"svg-left"} icon={ChevronRight} mega/>
    </div>
  }

  return <div className="card-header">{ props.text }</div>
};

export default CardHeader;