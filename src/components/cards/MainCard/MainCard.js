import React, {Component} from 'react';
import CardHeader from './CardHeader';
import CardBody from './CardBody';
import CardWord from '../CardWord/CardWord';
import CardImage from '../CardImage/CardImage';
import classNames from 'classnames';
import './mainCard.css';

export default class MainCard extends Component {

  constructor(props){
    super(props);

    this.state = {
      isFlipped: this.props.isFlipped,
      position: this.props.position,
      fullScreen: this.props.fullScreen,
      selected: this.props.selected
    };
  }

  get classes () {
    return classNames({
      'card bg-light mb-3 card-3d shadow': true,
      'is-flipped': this.state.isFlipped,
      'left': this.state.position === 'left',
      'right': this.state.position === 'right',
      'card-full-screen': this.state.fullScreen
    })
  }

  rotate = () => {
   console.log('this', this);

   if (this.state.fullScreen){
     this.setState({
       isFlipped: !this.state.isFlipped
     })
   } else {
     this.props.setChoose();
   }

  };

  render() {
    return (
      <div className={this.classes} onClick={this.rotate}>

        {this.props.selected ? <CardHeader/> : <CardHeader text={this.props.header}/>}

        <CardBody>

          {this.props.selected ?
            <CardWord side={'front'} word={this.props.word ? this.props.word : 'table'} lang={'rus'}/> :
            <CardImage src={this.props.imgBackground}/>
          }

          <CardWord side={'back'} word={this.props.word ? this.props.word : 'table'} lang={'eng'}/>

        </CardBody>
      </div>
    );
  }
};