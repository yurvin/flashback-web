import React from 'react';

const CardBody = (props) => {
  return (
    <span>
      { props.children.map(child => child) }
    </span>
  );
};

export default CardBody;