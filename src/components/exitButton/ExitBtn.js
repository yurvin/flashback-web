import React from 'react';
import Octicon, { SignOut } from '@githubprimer/octicons-react'; //https://insin.github.io/react-octicon/

const ExitBtn = (props) => {
  let exit = () => props.exitSession();

  return (
    <div onClick={exit} className={"float-right h-100"}>
      <Octicon className={"svg-center red"} icon={SignOut}></Octicon>
    </div>
  );
};

export default ExitBtn;
