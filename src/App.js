import React, { Component } from 'react';
import AuthorizationWrapper from './components/registration/AuthorizationWrapper';
import Main from './components/Main';
import ServerApi from './utils/ServerApi';
import Cookie from './utils/Cookie';

class App extends Component {

  constructor(props){
   super(props);

   this.state = {
     user: Cookie.get('user'),
     errAuth: Cookie.get('err')
     // user: 'iurii'
   };

    if(this.state.user){
      Cookie.delete('err');
      this.state.errAuth = Cookie.get('err');
    }

    this.serverApi = new ServerApi(this.state.user);
  }


  updateUser = (response) => {
    if(response.status === 200){
      Cookie.delete('err');
      Cookie.set('user', response.user);
    } else {
      Cookie.delete("user");
      Cookie.set('err', response.status);
    }

    this.setState({
      user: Cookie.get("user"),
      errAuth: Cookie.get('err')
    });
  };

  exitSession = () => {
    Cookie.delete("user");
    Cookie.delete('err');
    this.setState({ user: null });
  };

  static getDerivedStateFromProps(nextProps, prevState){
    prevState.user = Cookie.get('user');
    prevState.errAuth = Cookie.get('err');

    if(prevState.user){
      Cookie.delete('err');
      prevState.errAuth = Cookie.get('err');
    }

    return { ...prevState };
  }

  render() {
    return this.state.user ?
      <Main
        serverApi={this.serverApi}
        user={this.state.user}
        exitSession={this.exitSession}
      /> :
      <AuthorizationWrapper
        serverApi={this.serverApi}
        updateUser={this.updateUser}
        isError={this.state.errAuth==="401"}
      />
  }
}

export default App;