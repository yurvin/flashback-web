export default class Utils {

  static dataSampling(base, data) {
    let dataSample = [];

    data.forEach((obj)=>{
      let newObj = {};

      for(let key in base){
        if( !base.hasOwnProperty( key ) ) continue;

        if(!obj[key]){
          if(Utils.isNumeric(base[key])){
            obj[key] = Math.random();
          } else if (typeof base[key] === 'string'){
            obj[key] = "";
          }
        }
        newObj[key] = obj[key];
      }

      dataSample.push(newObj);
    });

    return dataSample.slice();
  }

  static isNumeric(n){
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  static iterObject(obj, func){
    if (typeof obj !== 'object' || typeof func !== 'function'){
      throw new Error ('iterObject: wrong arguments');
    }

    for(let key in obj) {
      if (obj.hasOwnProperty(key)) {
        func(key);
      }
    }
  }

  /**
   *
   input: 1e+30
   longnumberstring: 1000000000000000000000000000000
   to Number: 1e+30
   input: 1.456789123456e-30
   longnumberstring: 0.000000000000000000000000000001456789123456
   to Number: 1.456789123456e-30
   input: 1.456789123456e+30
   longnumberstring: 1456789123456000000000000000000
   to Number: 1.456789123456e+30
   input: 1e+80 longnumberstring: 100000000000000000000000000000000000000000000000000000000000000000000000000000000
   to Number: 1e+80

   * @param n
   * @return {string}
   */
  static longNumberString(n){
    if(n.toString().indexOf('e')===-1){
      return n;
    }

    n = Number(n);

    let str,
      str2= '',
      mag,
      data= n.toExponential().replace('.','').split(/e/i);

    str= data[0];
    mag= Number(data[1]);

    if(mag>=0 && str.length> mag){
      mag+=1;
      return str.substring(0, mag)+'.'+str.substring(mag);
    }
    if(mag<0){
      while(++mag) str2+= '0';
      return '0.'+str2+str;
    }

    mag= (mag-str.length)+1;

    while(mag> str2.length){
      str2+= '0';
    }

    return str+str2;
  }
};