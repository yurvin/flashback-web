import 'whatwg-fetch';
import Utils from './Utils';
import CONSTANTS from './constants.json';

const SERVER_URL = CONSTANTS.server.production.url;
const baseFileds = {
  "id": Math.random(),
  "eng": "",
  "transcription": "",
  "rus": ""
};

export default class ServerApi {
  constructor(user){
    this.user = user;
  }

  CRUD = {

    //create table
    create(user, data){
      console.log("CRUD: create method, data:", data);
    },

    //update table
    update(user, data){
      let words = Utils.dataSampling(baseFileds, data);
      let url = `${SERVER_URL}update/?user=${user}`;
      let options = {
        method:"post",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(words)
      };

      return fetch(url, options)
        .then(function(res){ return res; })
        .then(function(data){ console.log( JSON.stringify( data ) ) });
    },

    //get table by username after auth
    read(user) {
      let url = `${SERVER_URL}read/?user=${user}`;
      return fetch(url, {method: 'get'}).then(response => response.json());
    },

    //delete all table for current user
    delete(user) {
      console.log("CRUD: delete method, data:");
    }
  };

  authorization(username, password, callback){
    let url = SERVER_URL+"authorization/",
        options = {
          method: 'get',
          headers: {
            "Content-Type": "text/plain",
            'Authorization': 'Basic ' + btoa(username + ":" + password),
          }
        },
      result = {};

    fetch(url, options).then(function(response) {
      result.status = response.status;
      return response.json();
    }).then((data)=> {
      result = {...result, ...data};
      callback && callback(result);
    });
  }

  registration(username, password, callback){
    let url = SERVER_URL+"registration/",
      options = {
        method: 'post',
        headers: {
          "Content-Type": "text/plain",
          'Authorization': 'Basic ' + btoa(username + ":" + password),
        }
      },
      result = {};

    fetch(url, options).then(function(response) {
      result.status = response.status;
      return response.json();
    }).then((data)=> {
      result = {...result, ...data};
      callback && callback(result);
    });
  }
}
